package com.example.nail.beatbox

import android.content.Context
import android.media.AudioAttributes
import android.media.AudioManager
import android.media.SoundPool

class BeatBox(context: Context) {

	private val assetManager = context.assets
	private val soundPool: SoundPool
	var soundSpeed = 1.0f

	init {
		soundPool = SoundPool.Builder()
				.setMaxStreams(MAX_SOUNDS)
				.setAudioAttributes(
						AudioAttributes.Builder()
								.setLegacyStreamType(AudioManager.STREAM_MUSIC).build())
				.build()
	}

	companion object {
		private const val SOUNDS_FOLDER: String = "sample_sounds"
		private const val MAX_SOUNDS: Int = 5
	}

	fun loadSounds(): List<Sound> {
		return assetManager.list(SOUNDS_FOLDER)
				.map { soundByAssetPath("$SOUNDS_FOLDER/$it") }
				.onEach { load(it) }
				.toList()
	}

	fun play(sound: Sound) {
		sound.soundId?.let {
			soundPool.play(it, 1f, 1f, 1, 0, soundSpeed)
		}
	}

	private fun load(sound: Sound) {
		val afd = assetManager.openFd(sound.path)
		val soundId = soundPool.load(afd, 1)
		sound.soundId = soundId
	}

	fun release() {
		soundPool.release()
	}

}