package com.example.nail.beatbox

import android.databinding.BaseObservable
import android.databinding.Bindable

class SoundViewModel(private val beatBox: BeatBox) : BaseObservable() {

	var sound: Sound? = null
		set(value) {
			field = value
			notifyPropertyChanged(BR.title)
		}

	@get:Bindable
	val title: String?
		get() = sound?.name

	fun onButtonClicked() {
		sound?.let {
			beatBox.play(it)
		}
	}
}