package com.example.nail.beatbox

import android.arch.lifecycle.LiveData
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.nail.beatbox.databinding.FragmentBeatBoxBinding
import com.example.nail.beatbox.databinding.ListItemSoundBinding
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers.mainThread
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.schedulers.Schedulers.io
import kotlinx.android.synthetic.main.fragment_beat_box.*

class BeatBoxFragment : Fragment() {

	private lateinit var mAdapter: SoundAdapter
	private lateinit var mBeatBox: BeatBox

	private lateinit var mSingleGetSounds: Single<List<Sound>>
	private val mDisposables = CompositeDisposable()

	private lateinit var mBindingBeatBox: FragmentBeatBoxBinding

	companion object {
		fun newInstance(): BeatBoxFragment {
			return BeatBoxFragment()
		}
	}

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)

		val ctx = activity ?: throw IllegalStateException("activity is null")
		retainInstance = true

		mBeatBox = BeatBox(ctx)

		mAdapter = SoundAdapter(mBeatBox)

		mSingleGetSounds = Single.fromCallable { mBeatBox.loadSounds() }
				.subscribeOn(io())
				.observeOn(mainThread())
	}

	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
		mBindingBeatBox = DataBindingUtil.inflate(inflater,
				R.layout.fragment_beat_box, container, false)
		return mBindingBeatBox.root
	}

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)

		with(mBindingBeatBox.recyclerView) {
			adapter = mAdapter
			layoutManager = GridLayoutManager(activity, 3)
		}

		with(mBindingBeatBox.soundSpeed) {
			progress = 50
			onSeekBarChanged { mBeatBox.soundSpeed = (50 + it.toFloat()) / 100 }
		}
	}

	override fun onResume() {
		super.onResume()
		mDisposables += mSingleGetSounds.doOnSuccess { mAdapter.setSounds(it) }
				.subscribe()
	}

	override fun onStop() {
		super.onStop()
		mDisposables.clear()
	}

	override fun onDestroy() {
		super.onDestroy()
		mBeatBox.release()
	}

	class SoundAdapter(private val beatBox: BeatBox) : RecyclerView.Adapter<SoundHolder>() {
		private var sounds = listOf<Sound>()

		override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = SoundHolder(
				DataBindingUtil.inflate(
						LayoutInflater.from(parent.context),
						R.layout.list_item_sound, parent, false), beatBox)

		override fun getItemCount() = sounds.size

		override fun onBindViewHolder(holder: SoundHolder, position: Int) = holder.bind(sounds[position])

		fun setSounds(newSounds: List<Sound>) {
			sounds = newSounds
			notifyDataSetChanged()
		}

	}

	class SoundHolder(private val soundBinding: ListItemSoundBinding, beatBox: BeatBox)
		: RecyclerView.ViewHolder(soundBinding.root) {

		init {
			soundBinding.viewModel = SoundViewModel(beatBox)
		}

		fun bind(sound: Sound) {
			with(soundBinding) {
				viewModel!!.sound = sound
				this.executePendingBindings()
			}
		}
	}
}