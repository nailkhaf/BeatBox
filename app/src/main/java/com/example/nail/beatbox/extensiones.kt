package com.example.nail.beatbox

import android.widget.SeekBar

fun SeekBar.onSeekBarChanged(action: (Int) -> Unit) {
	setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
		override fun onProgressChanged(p0: SeekBar?, p1: Int, p2: Boolean) {
			action(p1)
		}

		override fun onStartTrackingTouch(p0: SeekBar?) {
		}

		override fun onStopTrackingTouch(p0: SeekBar?) {
		}

	})
}
