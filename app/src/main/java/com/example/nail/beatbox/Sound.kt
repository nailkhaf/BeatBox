package com.example.nail.beatbox

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

data class Sound(val name: String, val path: String, var soundId: Int? = null)

fun soundByAssetPath(assetPath: String): Sound {
	val name = assetPath.split("/").last().replace(".wav", "")
	return Sound(name, assetPath)
}